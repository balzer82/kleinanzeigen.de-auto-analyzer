import streamlit as st
import pandas as pd
import plotly.express as px

from kleinanzeigen import scrape

title = 'kleinanzeigen.de Auto Analyzer'
st.set_page_config(page_title=title)
st.title(title)

url = 'https://www.kleinanzeigen.de/s-autos/'
url += '{fahrzeugname}/'.format(fahrzeugname='bmw-z4')
url += '{cat}'.format(cat='k0c216')
url += '+autos.ez_i:{von}%2C{bis}'.format(von=2009, bis=2016)
url += '+autos.power_i:{mindestleistung}%2C'.format(mindestleistung=300)

st.session_state.url = st.session_state.get('url', url)

st.text_input(label='kleinanzeigen.de Auto Suche URL (copy & paste from Browser):',
              key='url', on_change=scrape(st.session_state.url))


def read_data() -> pd.DataFrame:
    df = pd.read_csv('result.csv')
    return df.sort_values(by='Kilometer')


data = read_data()

if len(data) > 0:

    fig = px.scatter(
        data,
        x="Kilometer",
        y="Preis",
        color="Baujahr",
        hover_data=['Name'],
        trendline='lowess'
    )
    fig.update_traces(marker={'size': 15})

    event = st.plotly_chart(fig, on_select="rerun", selection_mode='lasso')

    nothing_selected = len(event.selection.point_indices) == 0  # type: ignore
    filtered_data = data if nothing_selected else data.iloc[event.selection.point_indices]  # type: ignore

    st.dataframe(filtered_data,
                 use_container_width=True,
                 column_config={
                     "Name": st.column_config.TextColumn("Name", width='large'),
                     "Kilometer": st.column_config.NumberColumn('Kilometer'),
                     "Baujahr": st.column_config.NumberColumn('Baujahr', format='%d'),
                     "Preis": st.column_config.NumberColumn('Preis €'),
                     "url": st.column_config.LinkColumn("Link")},
                 hide_index=True)

else:

    st.markdown('''Nothing found. Check the URL above. Something like this works:
                
* https://www.kleinanzeigen.de/s-autos/bmw/z4/k0c216
* https://www.kleinanzeigen.de/s-autos/bmw/z4/k0c216+autos.ez_i:%2C2012+autos.marke_s:bmw+autos.model_s:z_reihe+autos.power_i:300%2C
                ''')
