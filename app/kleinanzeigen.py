import requests
from bs4 import BeautifulSoup

COLS = f"Name,Kilometer,Preis,Baujahr,url\n"


def scrape(url, csvfilename='result.csv'):

    with open(csvfilename, 'w') as csvfile:
        csvfile.write(COLS)

    if not 'https://www.kleinanzeigen.de/s-autos/' in url:
        return None

    # Send a GET request to the URL
    response = requests.get(url)

    # Check if the request was successful
    if not response.status_code == 200:
        return None

    # Parse the HTML content of the page
    soup = BeautifulSoup(response.content, 'html.parser')

    # Find all listings
    listings = soup.find_all('article', class_='aditem')

    with open(csvfilename, 'w') as csvfile:
        csvfile.write(COLS)

        # Loop through each listing to extract price and kilometers
        for listing in listings:

            # Extract Name
            listing_name = listing.find('a', class_='ellipsis', href=True)
            listing_url = 'https://kleinanzeigen.de' + listing_name['href']
            name = listing_name.text.strip('').replace(',', ' ')

            # Extract price
            price_element = listing.find('p', class_='aditem-main--middle--price-shipping--price')
            price = price_element.text.strip()
            price = price.replace('.', '')
            price = price.replace(' VB', '')
            price = price.replace(' €', '')

            # Extract kilometers
            simpletags = listing.find_all('span', class_='simpletag')
            for st in simpletags:
                tag = st.text.strip()
                if 'km' in tag:
                    km = tag.replace('.', '')
                    km = km.replace(' km', '')
                elif 'EZ' in tag:
                    baujahr = tag.replace('EZ ', '')
                    baujahr = baujahr.split('/')[-1]
                else:
                    print(f"unknown tag: {st}")

            # Print the extracted details
            # print(f"{name=}: {price=}, {km=}, {baujahr=}")

            csvfile.write(f"{name},{km},{price},{baujahr},{listing_url}\n")
