# kleinanzeigen.de Auto Analyzer
Streamlit app to analyze all vehicle search results from kleinanzeigen.de

## Requirements

`pip3 install -r requirements.txt`

## Run

`streamlit run app/main.py`

## What it does

It gets all listings from your vehicle search from kleinanzeigen.de and creates
a scatter plot with "Kilometer" (the lower the better) and "Preis" (the lower the better)
and calculates a linear fitting.
All vehicles below the line are maybe a good offer.

You can select (Lasso) some of them and get a list of your favorites.

![Screenshot](screenshot.png)